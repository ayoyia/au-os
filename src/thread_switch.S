.text
.global switch_threads
switch_threads:
	pushq %rbx
	pushq %rbp
	pushq %r12
	pushq %r13
	pushq %r14
	pushq %r15

	movq %rsp, (%rdi)
	movq %rsi, %rsp

	popq %r15
	popq %r14
	popq %r13
	popq %r12
	popq %rbp
	popq %rbx

	ret


.extern dummy

.global dummy_asm
dummy_asm:
	movq %rbx, %rdi
	cld
	call dummy
	ret