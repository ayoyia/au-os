#include <myio.h>
#include <ioport.h>

void printf(const char* format, ...) {
	va_list args;
	va_start(args, format);
	vprintf(format, args);
	va_end(args);
}

void vprintf(const char* format, va_list args) {
	while (*format) {
		char c = *(format++);
		if (c != '%') {
			write_to_serial_char(c);
			continue;
		}
		char cmd = *(format++);
		if (cmd == 'd' || cmd == 'i') {
			write_to_serial_d(va_arg(args, int));
		} else if (cmd == 'u') {
			write_to_serial_u(va_arg(args, unsigned int));
		} else if (cmd == 'c') {
			write_to_serial_char((char) (va_arg(args, int)));
		} else if (cmd == 's') {
			write_to_serial(va_arg(args, char*));
		} else if (cmd == 'o') {
			write_to_serial_o(va_arg(args, unsigned int));
		} else if (cmd == 'x') {
			write_to_serial_x(va_arg(args, unsigned int));
		} else if (cmd == 'h') {
			char cmd2 = *(format++);
			if (cmd2 == 'd' || cmd2 == 'i') {
				write_to_serial_d(va_arg(args, int));
			} else if (cmd2 == 'u') {
				write_to_serial_u(va_arg(args, int));
			} else if (cmd2 == 'o') {
				write_to_serial_o(va_arg(args, int));
			} else if (cmd2 == 'x') {
				write_to_serial_x(va_arg(args, int));
			} else if (cmd2 == 'h') {
				char cmd3 = *(format++);
				if (cmd3 == 'd' || cmd3 == 'i') {
					write_to_serial_d(va_arg(args, int));
				} else if (cmd3 == 'u') {
					write_to_serial_u(va_arg(args, int));
				} else if (cmd3 == 'o') {
					write_to_serial_o(va_arg(args, int));
				} else if (cmd3 == 'x') {
					write_to_serial_x(va_arg(args, int));
				}
			}
		} else if (cmd == 'l') {
			char cmd2 = *(format++);
			if (cmd2 == 'd' || cmd2 == 'i') {
				write_to_serial_d(va_arg(args, long));
			} else if (cmd2 == 'u') {
				write_to_serial_u(va_arg(args, long));
			} else if (cmd2 == 'o') {
				write_to_serial_o(va_arg(args, long));
			} else if (cmd2 == 'x') {
				write_to_serial_x(va_arg(args, long));
			} else if (cmd2 == 'l') {
				char cmd3 = *(format++);
				if (cmd3 == 'd' || cmd3 == 'i') {
					write_to_serial_d(va_arg(args, long long));
				} else if (cmd3 == 'u') {
					write_to_serial_u(va_arg(args, long long));
				} else if (cmd3 == 'o') {
					write_to_serial_o(va_arg(args, long long));
				} else if (cmd3 == 'x') {
					write_to_serial_x(va_arg(args, long long));
				}
			}
		} 
	}
}

int snprintf(char* s, size_t n, const char* format, ...) {
	va_list args;
	va_start(args, format);
	int res = vsnprintf(s, n, format, args);
	va_end(args);
	return res;
}

int vsnprintf(char* s, size_t n, const char* format, va_list args) {
	size_t pos = 0;
	while (*format) {
		char c = *(format++);
		if (c != '%') {
			write_to_serial_char_buf(s, &pos, n, c);
			continue;
		}
		char cmd = *(format++);
		if (cmd == 'd' || cmd == 'i') {
			write_to_serial_d_buf(s, &pos, n, va_arg(args, int));
		} else if (cmd == 'u') {
			write_to_serial_u_buf(s, &pos, n, va_arg(args, unsigned int));
		} else if (cmd == 'c') {
			write_to_serial_char_buf(s, &pos, n, (char) (va_arg(args, int)));
		} else if (cmd == 's') {
			write_to_serial_buf(s, &pos, n, va_arg(args, char*));
		} else if (cmd == 'o') {
			write_to_serial_o_buf(s, &pos, n, va_arg(args, unsigned int));
		} else if (cmd == 'x') {
			write_to_serial_x_buf(s, &pos, n, va_arg(args, unsigned int));
		} else if (cmd == 'h') {
			char cmd2 = *(format++);
			if (cmd2 == 'd' || cmd2 == 'i') {
				write_to_serial_d_buf(s, &pos, n, va_arg(args, int));
			} else if (cmd2 == 'u') {
				write_to_serial_u_buf(s, &pos, n, va_arg(args, int));
			} else if (cmd2 == 'o') {
				write_to_serial_o_buf(s, &pos, n, va_arg(args, int));
			} else if (cmd2 == 'x') {
				write_to_serial_x_buf(s, &pos, n, va_arg(args, int));
			} else if (cmd2 == 'h') {
				char cmd3 = *(format++);
				if (cmd3 == 'd' || cmd3 == 'i') {
					write_to_serial_d_buf(s, &pos, n, va_arg(args, int));
				} else if (cmd3 == 'u') {
					write_to_serial_u_buf(s, &pos, n, va_arg(args, int));
				} else if (cmd3 == 'o') {
					write_to_serial_o_buf(s, &pos, n, va_arg(args, int));
				} else if (cmd3 == 'x') {
					write_to_serial_x_buf(s, &pos, n, va_arg(args, int));
				}
			}
		} else if (cmd == 'l') {
			char cmd2 = *(format++);
			if (cmd2 == 'd' || cmd2 == 'i') {
				write_to_serial_d_buf(s, &pos, n, va_arg(args, long));
			} else if (cmd2 == 'u') {
				write_to_serial_u_buf(s, &pos, n, va_arg(args, long));
			} else if (cmd2 == 'o') {
				write_to_serial_o_buf(s, &pos, n, va_arg(args, long));
			} else if (cmd2 == 'x') {
				write_to_serial_x_buf(s, &pos, n, va_arg(args, long));
			} else if (cmd2 == 'l') {
				char cmd3 = *(format++);
				if (cmd3 == 'd' || cmd3 == 'i') {
					write_to_serial_d_buf(s, &pos, n, va_arg(args, long long));
				} else if (cmd3 == 'u') {
					write_to_serial_u_buf(s, &pos, n, va_arg(args, long long));
				} else if (cmd3 == 'o') {
					write_to_serial_o_buf(s, &pos, n, va_arg(args, long long));
				} else if (cmd3 == 'x') {
					write_to_serial_x_buf(s, &pos, n, va_arg(args, long long));
				}
			}
		} 
	}
	if (pos <= n) {
		return 0;
	}
	return pos;
}

void write_to_serial_char(char c) {
	int ok;
	do {
		unsigned short port5 = in8(SERIAL_PREFIX + 5);
		ok = (port5 & (1 << 5));
	} while (!ok);
	out8(SERIAL_PREFIX + 0, c);
}

void write_to_serial(const char* s) {
	while (*s) {
		write_to_serial_char(*s);
		++s;
	}
}

void reverse_buf(char* buf, size_t size) {
	size_t i = 0;
	for (i = 0; i * 2 < size; i++) {
		char c = buf[i];
		buf[i] = buf[size - 1 - i];
		buf[size - 1 - i] = c;
	}
}

void write_to_serial_u(uint64_t x) {
	char buf[100];
	size_t len = 0;
	do {
		buf[len++] = x % 10 + '0';
		x /= 10;
	} while (x != 0);
	reverse_buf(buf, len);
	buf[len] = 0;
	write_to_serial(buf);
}

void write_to_serial_d(int64_t x) {
	if (x < 0) {
		write_to_serial_char('-');
		x = -x;
	}
	write_to_serial_u((uint64_t) x);
}

void write_to_serial_o(uint64_t x) {
	char buf[100];
	size_t len = 0;
	do {
		buf[len++] = x % 8 + '0';
		x /= 8;
	} while (x != 0);
	reverse_buf(buf, len);
	buf[len] = 0;
	write_to_serial(buf);	
}

void write_to_serial_x(uint64_t x) {
	char buf[100];
	size_t len = 0;
	do {
		char c = x % 16;
		if (c < 10) {
			c += '0';
		} else {
			c -= 10;
			c += 'a';
		}
		x /= 16;
		buf[len++] = c;
	} while (x != 0);
	reverse_buf(buf, len);
	buf[len] = 0;
	write_to_serial(buf);		
}

void write_to_serial_char_buf(char* buf, size_t* pos, size_t n, char c) {
	if (*pos < n) {
		buf[*pos] = c;
	}
	++(*pos);
}

void write_to_serial_buf(char* buf, size_t* pos, size_t n, const char* s) {
	while (*s) {
		write_to_serial_char_buf(buf, pos, n, *s);
		++s;
	}
}

void write_to_serial_u_buf(char* b, size_t* pos, size_t n, uint64_t x) {
	char buf[100];
	size_t len = 0;
	do {
		buf[len++] = x % 10 + '0';
		x /= 10;
	} while (x != 0);
	reverse_buf(buf, len);
	buf[len] = 0;
	write_to_serial_buf(b, pos, n, buf);
}

void write_to_serial_d_buf(char* b, size_t* pos, size_t n, int64_t x) {
	if (x < 0) {
		write_to_serial_char_buf(b, pos, n, '-');
		x = -x;
	}
	write_to_serial_u_buf(b, pos, n, (uint64_t) x);
}

void write_to_serial_o_buf(char* b, size_t* pos, size_t n, uint64_t x) {
	char buf[100];
	size_t len = 0;
	do {
		buf[len++] = x % 8 + '0';
		x /= 8;
	} while (x != 0);
	reverse_buf(buf, len);
	buf[len] = 0;
	write_to_serial_buf(b, pos, n, buf);
}

void write_to_serial_x_buf(char* b, size_t* pos, size_t n, uint64_t x) {
	char buf[100];
	size_t len = 0;
	do {
		char c = x % 16;
		if (c < 10) {
			c += '0';
		} else {
			c -= 10;
			c += 'a';
		}
		x /= 16;
		buf[len++] = c;
	} while (x != 0);
	reverse_buf(buf, len);
	buf[len] = 0;
	write_to_serial_buf(b, pos, n, buf);
}