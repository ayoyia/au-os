#include <cartesian.h>
#include <mrand.h>
#include <stddef.h>
#include <myio.h>
#include <threads.h>

struct cart_node cart_nodes[CART_NODES_CNT * 2];

ptreap free_cart_nodes[CART_NODES_CNT * 2];
ptreap cart_node_by_addr[CART_NODES_CNT];
ptreap card_node_by_len[CART_NODES_CNT];

size_t cnt_free_cart_nodes = 0;

ptreap by_addr_root = NULL;
ptreap by_len_root = NULL;

uint64_t cart_node_addrs[CART_NODES_CNT];
uint64_t cart_node_lens[CART_NODES_CNT];

void init_cartesian() {
	for (size_t i = 0; i < CART_NODES_CNT; i++) {
		cart_nodes[2 * i].id = i;
		cart_nodes[2 * i + 1].id = i;

		cart_nodes[2 * i].y = mrand();
		cart_nodes[2 * i + 1].y = mrand();

		free_cart_nodes[cnt_free_cart_nodes++] = &cart_nodes[2 * i];
		free_cart_nodes[cnt_free_cart_nodes++] = &cart_nodes[2 * i + 1];
	}
}

void cart_split_with_id(ptreap t, uint64_t key, size_t id, ptreap* l, ptreap* r) {
	if (t == NULL) {
		(*l) = NULL;
		(*r) = NULL;
		return;
	}
	if (key < t->key || (key == t->key && id <= t->id)) {
		cart_split_with_id(t->l, key, id, l, r);
		t->l = *r;
		(*r) = t;
	} else {
		cart_split_with_id(t->r, key, id, l, r);
		t->r = *l;
		(*l) = t;
	}
}

void cart_split(ptreap t, uint64_t key, ptreap* l, ptreap* r) {
	cart_split_with_id(t, key, 0, l, r);
}

void cart_merge(ptreap a, ptreap b, ptreap* t) {
	if (a == NULL) {
		(*t) = b;
	} else if (b == NULL) {
		(*t) = a;
	} else {
		if (a->y < b->y) {
			cart_merge(a->r, b, &(a->r));
			(*t) = a;
		} else {
			cart_merge(a, b->l, &(b->l));
			(*t) = b;
		}
	}
}

ptreap get_lowest(ptreap t) {
	if (t == NULL) {
		return NULL;
	}
	while (t->l != NULL) {
		t = t->l;
	}
	return t;
}

ptreap get_highest(ptreap t) {
	if (t == NULL) {
		return NULL;
	}
	while (t->r != NULL) {
		t = t->r;
	}
	return t;
}

ptreap cart_remove(ptreap* t, uint64_t key, size_t id) {
	ptreap a, b, c, d;
	cart_split_with_id(*t, key, id, &a, &b);
	cart_split_with_id(b, key, id + 1, &c, &d);
	cart_merge(a, d, t);
	return c;
}

void cart_insert(ptreap* t, ptreap node) {
	node->l = NULL;
	node->r = NULL;

	ptreap a, b, c;
	cart_split_with_id(*t, node->key, node->id, &a, &b);
	cart_merge(a, node, &c);
	cart_merge(c, b, t);
}

void remove_nodes_paired(size_t id) {
	free_cart_nodes[cnt_free_cart_nodes++] = cart_remove(&by_addr_root, cart_node_addrs[id], id);
	free_cart_nodes[cnt_free_cart_nodes++] = cart_remove(&by_len_root, cart_node_lens[id], id);
}

void insert_addr_len_paired(uint64_t addr, uint64_t len) {
	if (cnt_free_cart_nodes == 0) {
		printf("no more free nodes\n");
		while(1);
	}		
	ptreap n_addr = free_cart_nodes[--cnt_free_cart_nodes];
	ptreap n_len = free_cart_nodes[--cnt_free_cart_nodes];
	if (n_addr->id != n_len->id) {
		printf("insert ids not equal\n");
		while (1);
	}
	size_t id = n_addr->id;
	cart_node_addrs[id] = addr;
	cart_node_lens[id] = len;

	n_addr->key = addr;
	n_len->key = len;

	cart_insert(&by_addr_root, n_addr);
	cart_insert(&by_len_root, n_len);
}

struct spinlock system_cartesian_lock;

void free_mem_non_locked(uint64_t addr, uint64_t len) {
	if (addr % CART_MEMORY_ALIGNMENT != 0) {
		uint64_t add = CART_MEMORY_ALIGNMENT - addr % CART_MEMORY_ALIGNMENT;
		if (len <= add) {
			return;
		}
		len -= add;
		addr += add;
	}

	if (addr >= CART_MAX_MEMORY) {
		return;
	}
	if (addr + len >= CART_MAX_MEMORY) {
		len = CART_MAX_MEMORY - addr;
	}

	if (len <= CART_MEMORY_ALIGNMENT) {
		return;
	}

	ptreap a, b;
	cart_split(by_addr_root, addr, &a, &b);
	ptreap l = get_highest(a);
	ptreap r = get_lowest(b);
	cart_merge(a, b, &by_addr_root);

	if (l != NULL && cart_node_addrs[l->id] + cart_node_lens[l->id] == addr) {
		addr = cart_node_addrs[l->id];
		len += cart_node_lens[l->id];

		remove_nodes_paired(l->id);
	}

	if (r != NULL && addr + len == cart_node_addrs[r->id]) {
		len += cart_node_lens[r->id];

		remove_nodes_paired(r->id);
	}

	insert_addr_len_paired(addr, len);	
}

void free_mem(uint64_t addr, uint64_t len) {
	my_lock(&system_cartesian_lock);
	free_mem_non_locked(addr, len);
	my_unlock(&system_cartesian_lock);
}

uint64_t get_mem(uint64_t len) {
	my_lock(&system_cartesian_lock);
	ptreap a, b;
	cart_split(by_len_root, len, &a, &b);
	if (b == NULL) {
		printf("NO MEMORY!!!\n");
		while (1);
	}

	ptreap t = get_lowest(b);
	uint64_t addr = cart_node_addrs[t->id];
	uint64_t cur_len = cart_node_lens[t->id];

	remove_nodes_paired(t->id);

	if (cur_len > len) {
		free_mem_non_locked(addr + len, cur_len - len);
	}

	my_unlock(&system_cartesian_lock);
	return addr;
}