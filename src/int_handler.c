#include <ioport.h>
#include <myio.h>
#include <threads.h>

struct frame {
	uint64_t r15;
	uint64_t r14;
	uint64_t r13;
	uint64_t r12;
	uint64_t r11;
	uint64_t r10;
	uint64_t r9;
	uint64_t rsi;
	uint64_t rdi;
	uint64_t rbp;
	uint64_t rdx;
	uint64_t rcx;
	uint64_t rbx;
	uint64_t rax;	
	uint64_t intno;
	uint64_t error;
} __attribute__((packed));

void qemu_gdb_hang(void);

void c_handler(struct frame *f) {
	// qemu_gdb_hang();
	// write_to_serial("c_handler\n");
	// write_to_serial_d(f->intno);
	if (f->intno == 32) {
		out8(0x20, 0x60 + 0);
		switch_to_random_thread();
	}
}