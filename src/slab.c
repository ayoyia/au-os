#include <slab.h>
#include <cartesian.h>
#include <memory.h>
#include <myio.h>
#include <threads.h>

void init_slab(struct slab* res, size_t size) {
	res->size = size;
	res->free_blocks_cnt = 0;
	res->free_blocks = (uint8_t**) (get_mem(4096 * sizeof(uint8_t*)) + VIRTUAL_BASE);
	res->free_blocks_size = 4096;
	my_unlock(&res->slab_lock);
}

void free_addr_non_lock(struct slab* s, uint8_t* addr) {
	if (s->free_blocks_cnt == s->free_blocks_size) {
		size_t size = s->free_blocks_size;
		uint8_t** new_blocks = (uint8_t**) (get_mem(size * 2 * sizeof(uint8_t*)) + VIRTUAL_BASE);
		for (size_t i = 0; i < size; i++) {
			new_blocks[i] = s->free_blocks[i];
		}
		free_mem((uint64_t) s->free_blocks - VIRTUAL_BASE, sizeof(uint8_t*) * size);
		s->free_blocks = new_blocks;
		s->free_blocks_size *= 2;
	}
	s->free_blocks[s->free_blocks_cnt++] = addr;
}

void free_addr(struct slab* s, uint8_t* addr) {
	my_lock(&s->slab_lock);
	free_addr_non_lock(s, addr);
	my_unlock(&s->slab_lock);
}

uint8_t* get_addr(struct slab* s) {
	my_lock(&s->slab_lock);
	if (s->free_blocks_cnt == 0) {
		uint8_t* mem_block = (uint8_t*) (get_mem(s->size * 4096) + VIRTUAL_BASE);
		for (size_t i = 0; i < 4096; i++) {
			free_addr_non_lock(s, mem_block + i * s->size);
		}
	}	
	s->free_blocks_cnt--;
	uint8_t* res = s->free_blocks[s->free_blocks_cnt];
	my_unlock(&s->slab_lock);
	return res;
}

