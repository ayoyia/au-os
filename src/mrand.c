#include "mrand.h"

int mrand_seed = 31;

int mrand() {
	int res = 1103515245 * mrand_seed + 12345;
	mrand_seed = res;
	return res;
}