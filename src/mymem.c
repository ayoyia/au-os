#include <mboot.h>
#include <myio.h>
// #include <buddy.h>
#include <cartesian.h>

extern void qemu_gdb_hang(void);

struct memory_region {
	uint32_t size;
	uint64_t addr;
	uint64_t length;
	uint32_t type;
} __attribute__((packed));

struct memory_region mem_regs[100];
size_t mem_regs_cnt = 0;

void add_mem_reg(uint64_t addr, uint64_t length, uint32_t type) {
	if (length == 0) {
		return;
	}
	mem_regs[mem_regs_cnt].addr = addr;
	mem_regs[mem_regs_cnt].length = length;
	mem_regs[mem_regs_cnt].type = type;
	mem_regs_cnt++;
}

void init_mem(struct mboot_struct* mbs) {
	extern char text_phys_begin[];
	extern char bss_phys_end[];
	uintptr_t kernel_begin = (uintptr_t) text_phys_begin;
	uintptr_t kernel_end = (uintptr_t) bss_phys_end;

	for (size_t i = 0; i < mbs->mmap_length; ) {
		struct memory_region* cur = (struct memory_region*) ((char*) ((uintptr_t) mbs->mmap_addr) + i);

		if (cur->addr <= kernel_begin && kernel_end <= cur->addr + cur->length) {
			add_mem_reg(cur->addr, kernel_begin - cur->addr, cur->type);
			add_mem_reg(kernel_begin, kernel_end - kernel_begin, MMAP_REGION_RESERVED);
			add_mem_reg(kernel_end, cur->addr + cur->length - kernel_end, cur->type);
		} else {
			add_mem_reg(cur->addr, cur->length, cur->type);
		}

		i += cur->size + 4;
	}

	printf("regions: type, addr, length\n");
	for (size_t i = 0; i < mem_regs_cnt; i++) {
		printf("%u ", mem_regs[i].type);
		printf("%llu ", mem_regs[i].addr);
		printf("%llu\n", mem_regs[i].length);

		if (mem_regs[i].type == MMAP_REGION_FREE) {
			// buddy_add_region(mem_regs[i].addr, mem_regs[i].length);
			free_mem(mem_regs[i].addr, mem_regs[i].length);
		}
	}

}

