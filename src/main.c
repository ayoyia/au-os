void qemu_gdb_hang(void)
{
#ifdef DEBUG
	static volatile int wait = 1;

	while (wait);
#endif
}

#include <desc.h>
#include <ints.h>
#include <ioport.h>
#include <memory.h>
#include <myio.h>
#include <mboot.h>
// #include <buddy.h>
#include <cartesian.h>
#include <paging.h>
#include <myalloc.h>
#include <threads.h>


#define INTS_CNT 128

extern uint32_t mboot_ptr;

void init_serial(void) {
	out8(SERIAL_PREFIX + 3, 0);
	out8(SERIAL_PREFIX + 1, 0);
	out8(SERIAL_PREFIX + 3, 1 << 7);
	out8(SERIAL_PREFIX + 0, 4);
	out8(SERIAL_PREFIX + 1, 0);
	out8(SERIAL_PREFIX + 3, 3);
}

struct IDTDescr {
	uint16_t offset1;
	uint16_t selector;
	uint8_t zero1;
	uint8_t type;
	uint16_t offset2;
	uint32_t offset3;
	uint32_t zero2;
} __attribute__((packed));

struct IDTDescr idt_descrs[INTS_CNT];
extern uint64_t table[];

void set_one_descr(int i, uint64_t offset) {
	idt_descrs[i].offset1 = offset & 0xFFFF;
	idt_descrs[i].offset2 = (offset >> 16) & 0xFFFF;
	idt_descrs[i].offset3 = (offset >> 32) & 0xFFFFFFFFu;
	idt_descrs[i].zero1 = 0;
	idt_descrs[i].zero2 = 0;
	idt_descrs[i].selector = KERNEL_CS;
	idt_descrs[i].type = (1 << 7) | 0xF;
}

void init_idt_descr(void) {
	unsigned i;
	for (i = 0; i < INTS_CNT; i++) {
		set_one_descr(i, table[i]);
	}

	struct desc_table_ptr ptr = {INTS_CNT * 16 - 1, (uint64_t) &idt_descrs};
	write_idtr(&ptr);
}

void init_pic() {
	// master
	out8(0x20, 0x11);
	out8(0x21, 32);
	out8(0x21, 4);
	out8(0x21, 1);

	out8(0x21, 0xFE);

	// slave
	out8(0xA0, 0x11);
	out8(0xA1, 40);
	out8(0xA1, 2);
	out8(0xA1, 1);

	enable_ints();
}

void init_pit() {
	out8(0x43, (2 << 1) | (3 << 4));
	out8(0x40, 0xff);
	out8(0x40, 0xff);
}

void print2() {
	while (1) {
		printf("2");
	}
}

void print3() {
	while (1) {
		printf("3");
	}
}

void main(void)
{
	// (void) qemu_gdb_hang;

	init_serial();

	init_cartesian();
	// init_buddy();
	init_mem((struct mboot_struct*) ((uint64_t) mboot_ptr));
	init_paging();
	init_allocation();

	init_threads();
	printf("created thread %d\n", create_thread(&print2));
	printf("created thread %d\n", create_thread(&print3));
	// printf("%llx\n", print2);
	// printf("%llx\n", &print2);

	// while(1);

	init_idt_descr();

	init_pit();
	init_pic();

	


	// printf("Hello!\n");
	// printf("%llu\n", 42);
	// printf("%lld\n", mboot_ptr);


	// __asm__ ("int $77");


	while (1);
}
