#include <myalloc.h>
#include <cartesian.h>
#include <slab.h>
#include <myio.h>
#include <threads.h>

struct slab slabs[4096];
uint8_t slab_inited[4096];

struct spinlock myalloc_lock;

void init_allocation(void) {
	for (size_t i = 0; i < 4096; ++i) {
		slab_inited[i] = 0;
	}
}

void* myalloc(size_t bytes) {
	if (bytes == 0) {
		return NULL;
	}
	my_lock(&myalloc_lock);
	bytes += sizeof(size_t);
	if (bytes <= 4096) {
		size_t slab_id = bytes - 1;
		if (!slab_inited[slab_id]) {
			init_slab(slabs + slab_id, bytes);
			slab_inited[slab_id] = 1;
		}
		uint8_t* res = get_addr(slabs + slab_id);
		*((size_t*) res) = bytes;
		return res + sizeof(size_t);
	}

	if (bytes % CART_MEMORY_ALIGNMENT != 0) {
		bytes += CART_MEMORY_ALIGNMENT - bytes % CART_MEMORY_ALIGNMENT;
	}

	uint64_t addr = get_mem(bytes);

	uint8_t* ptr = (uint8_t*) addr;

	*((size_t*) ptr) = bytes;
	my_unlock(&myalloc_lock);
	return ptr + sizeof(size_t);
}

void myfree(void* ptr) {
	if (ptr == NULL) {
		return;
	}

	my_lock(&myalloc_lock);

	ptr = (uint8_t*) ptr - sizeof(size_t);
	size_t bytes = *((size_t*) ptr);
	if (bytes <= 4096) {
		if (!slab_inited[bytes - 1]) {
			printf("slab %u not inited\n", bytes);
			while (1);
		}
		free_addr(slabs + bytes, ptr);
	} else {
		if (bytes % CART_MEMORY_ALIGNMENT != 0) {
			bytes += CART_MEMORY_ALIGNMENT - bytes % CART_MEMORY_ALIGNMENT;
		}
		free_mem((uint64_t) ptr, bytes);
	}

	my_unlock(&myalloc_lock);
}