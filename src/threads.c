#include <threads.h>
#include <myalloc.h>
#include <myio.h>
#include <mrand.h>
#include <ints.h>

void my_lock(struct spinlock* lock) {
	while (atomic_exchange_explicit(&lock->locked, LOCKED, memory_order_acquire) == LOCKED);
}

void my_unlock(struct spinlock* lock) {
	atomic_store_explicit(&lock->locked, UNLOCKED, memory_order_release);
}

struct thread threads[MAX_THREADS];
size_t current_thread_id;

extern struct spinlock system_cartesian_lock;
struct spinlock system_thread_lock;

void init_global_locks() {
	my_unlock(&system_cartesian_lock);
	my_unlock(&system_thread_lock);
}

void init_threads() {
	for (int i = 0; i < MAX_THREADS; i++) {
		threads[i].state = STATE_DEAD;
		threads[i].waiter_id = NO_WAITER;
	}

	current_thread_id = 0;
	threads[0].state = STATE_ALIVE;

	init_global_locks();
}

void dummy(uint64_t df) {
	// printf("dummy\n");
	// while(1);
	enable_ints();
	// printf("%llx\n", df);
	(*((void (*)()) df))();
}

extern void dummy_asm();

size_t create_thread(void (*fun)()) {
	my_lock(&system_thread_lock);
	// printf("%llx\n", fun);
	for (int i = 0; i < MAX_THREADS; i++) {
		if (threads[i].state == STATE_DEAD) {
			threads[i].sp = (void*) ((uint8_t*) threads[i].stack + THREAD_STACK_SIZE);
			*((uint64_t*) threads[i].sp - 1) = (uint64_t) dummy_asm;
			*((uint64_t*) threads[i].sp - 2) = (uint64_t) fun;
			threads[i].sp = (void*) ((uint64_t*) threads[i].sp - 7);
			threads[i].waiter_id = NO_WAITER;
			threads[i].state = STATE_ALIVE;
			my_unlock(&system_thread_lock);
			return i;
		}
	}
	printf("Threads number reached its limit\n");
	while (1);
}

void wait_thread(size_t me, size_t him) {
	my_lock(&system_thread_lock);
	if (threads[him].state == STATE_DEAD) {
		my_unlock(&system_thread_lock);
		return;
	}
	threads[me].state = STATE_WAIT;
	threads[him].waiter_id = me;
	my_unlock(&system_thread_lock);
	switch_to_random_thread();
}

void kill_thread(size_t tid) {
	my_lock(&system_thread_lock);
	threads[tid].state = STATE_DEAD;
	if (threads[tid].waiter_id != NO_WAITER) {
		threads[threads[tid].waiter_id].state = STATE_ALIVE;
	}
	my_unlock(&system_thread_lock);
	switch_to_random_thread();
}

void switch_threads_by_id(size_t prev_id, size_t next_id) {
	current_thread_id = next_id;
	switch_threads(&threads[prev_id].sp, threads[next_id].sp);
}

void switch_to_random_thread() {
	disable_ints();
	size_t new_id = current_thread_id;
	for (size_t i = 1; i < MAX_THREADS; i++) {
		if (threads[(i + current_thread_id) % MAX_THREADS].state == STATE_ALIVE) {
			new_id = (i + current_thread_id) % MAX_THREADS;
			break;
		}
	}
	// printf("\n\n\n\nnew_thread_id %d\n\n\n\n", new_id);
	if (current_thread_id != new_id) {
		switch_threads_by_id(current_thread_id, new_id);
	}
	enable_ints();
}

void init_global_locks();