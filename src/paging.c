#include <paging.h>
#include <stdint.h>
#include <stddef.h>
#include <memory.h>
#include <myio.h>

extern void qemu_gdb_hang(void);

uint64_t phys_page_1[512] __attribute__((aligned(4096)));
uint64_t phys_page_2[512] __attribute__((aligned(4096)));
uint64_t phys_page_3[512] __attribute__((aligned(4096)));

void zero_page(uint64_t* a) {
	for (size_t i = 0; i < 512; i++) {
		a[i] = 0;
	}
}

void init_paging(void) {
	zero_page(phys_page_1);
	zero_page(phys_page_2);
	zero_page(phys_page_3);

	phys_page_1[256] = ((uint64_t)phys_page_2 - VIRTUAL_BASE) | 7ull;
	phys_page_1[511] = ((uint64_t)phys_page_3 - VIRTUAL_BASE) | 7ull;

	for (size_t i = 0; i != 512; ++i)
		phys_page_2[i] = (uint64_t)i * (1ull << 30) | 135ull;

	phys_page_3[511] = (1ull << 30) | 135ull;
	phys_page_3[510] = 135ull;

	// qemu_gdb_hang();

	__asm__ ("movq %0, %%cr3" : : "r"((uint64_t)phys_page_1 - VIRTUAL_BASE));
}