#ifndef __MYIO_H__
#define __MYIO_H__

#include <stdint.h>
#include <stddef.h>
#include <stdarg.h>

void printf(const char* format, ...);
void vprintf(const char* format, va_list arg);
int snprintf(char* s, size_t n, const char* format, ...);
int vsnprintf(char* s, size_t n, const char* format, va_list arg);

void write_to_serial_char(char c);
void write_to_serial(const char* s);
void write_to_serial_d(int64_t d);
void write_to_serial_u(uint64_t u);
void write_to_serial_o(uint64_t o);
void write_to_serial_x(uint64_t x);
void write_to_serial_char_buf(char* b, size_t* pos, size_t n, char c);
void write_to_serial_buf(char* b, size_t* pos, size_t n, const char* s);
void write_to_serial_d_buf(char* b, size_t* pos, size_t n, int64_t d);
void write_to_serial_u_buf(char* b, size_t* pos, size_t n, uint64_t u);
void write_to_serial_o_buf(char* b, size_t* pos, size_t n, uint64_t o);
void write_to_serial_x_buf(char* b, size_t* pos, size_t n, uint64_t x);

#endif /* __MYIO_H__ */