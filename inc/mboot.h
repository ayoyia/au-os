#ifndef __MBOOT_H__
#define __MBOOT_H__

#include <stdint.h>

#define MMAP_REGION_FREE 1
#define MMAP_REGION_RESERVED 2

struct mboot_struct {
	uint32_t flags;
	uint32_t mem_lower;
	uint32_t mem_upper;
	uint32_t boot_device;
	uint32_t cmdline;
	uint32_t mods_count;
	uint32_t mods_addr;
	uint64_t syms1;
	uint64_t syms2;
	uint32_t mmap_length;
	uint32_t mmap_addr;
	uint32_t drives_length;
	uint32_t drives_addr;
	uint32_t config_table;
	uint32_t bootloader_name;
	uint32_t apm_table;
	uint32_t vbe_control_info;
	uint32_t vbe_mode_info;
	uint32_t vbe_mode;
	uint32_t vbe_interface_seg;
	uint32_t vbe_interface_off;
	uint32_t vbe_interface_len;
} __attribute__((packed));


void init_mem(struct mboot_struct* mbs);

#endif //__MBOOT_H__