#ifndef __MYALLOC__
#define __MYALLOC__ value

#include <stddef.h>
#include <stdint.h>

void init_allocation(void);

void* myalloc(size_t bytes);
void myfree(void* ptr);

#endif