#ifndef __BUDDY_H__
#define __BUDDY_H__

#include <stdint.h>
#include <stddef.h>

#define BUDDY_ALIGNMENT 4096
#define BUDDY_LVLS_CNT 22

struct buddy_block {
	uint64_t addr;
	size_t lvl;
	size_t num;
	size_t id;
};

void init_buddy();
void buddy_add_region(uint64_t addr, uint64_t length);

#endif //__BUDDY_H__