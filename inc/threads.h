#ifndef __THREADS_H__
#define __THREADS_H__ 

#include <stdatomic.h>
#include <stddef.h>
#include <stdint.h>


#define MAX_THREADS 31
#define LOCKED 1
#define UNLOCKED 0
#define THREAD_STACK_SIZE 8192

#define STATE_DEAD 0
#define STATE_ALIVE 1
#define STATE_WAIT 2

#define NO_WAITER MAX_THREADS

struct spinlock
{
	atomic_int locked;
};

void my_lock(struct spinlock* obj);
void my_unlock(struct spinlock* obj);

void init_threads();

struct thread
{
	uint8_t stack[THREAD_STACK_SIZE];
	void* sp;
	int state;
	size_t waiter_id;
};

size_t create_thread(void (*fun)());
void wait_thread(size_t me, size_t him);
void kill_thread(size_t tid);

void switch_threads(void** prev, void* next);
void switch_threads_by_id(size_t prev_id, size_t next_id);
void switch_to_random_thread();

#endif /* __THREADS_H__ */