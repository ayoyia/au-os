#ifndef __CARTESIAN_H__
#define __CARTESIAN_H__

#include <stdint.h>
#include <stddef.h>

#define CART_MEMORY_ALIGNMENT 4096u
#define CART_NODES_CNT (1u << 16)

#define CART_MAX_MEMORY (1ull << 39)

struct cart_node {
	uint64_t key;
	struct cart_node *l, *r;
	int y;
	size_t id;
};

typedef struct cart_node* ptreap;

void init_cartesian();
void free_mem(uint64_t addr, uint64_t len);
uint64_t get_mem(uint64_t len);


#endif // __CARTESIAN_H__