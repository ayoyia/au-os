#ifndef __SLAB_H__
#define __SLAB_H__

#include <stddef.h>
#include <stdint.h>
#include <threads.h>

struct slab {
	size_t size;
	uint8_t** free_blocks;
	size_t free_blocks_size;
	size_t free_blocks_cnt;
	struct spinlock slab_lock;
};

void init_slab(struct slab* s, size_t size);

uint8_t* get_addr(struct slab* s);
void free_addr(struct slab* s, uint8_t* addr);

#endif /* __SLAB_H__ */