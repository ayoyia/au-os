#! /usr/bin/env python3

import sys
sys.stdout = open("src/entry.S", "w")

print(".text")
print(".extern c_handler")
print(".global table")
print("")

int_with_err_code = {8, 10, 11, 12, 13, 14, 17}

INTS_CNT = 128

for i in range(INTS_CNT):
	print("entry" + str(i) + ":")
	if i not in int_with_err_code:
		print(r"	subq $8, %rsp")
	print(r"	pushq $" + str(i))
	print(r"	jmp common")
	print("")


print(r"""common:
	pushq %rax
	pushq %rbx
	pushq %rcx
	pushq %rdx
	pushq %rbp
	pushq %rdi
	pushq %rsi
	pushq %r9
	pushq %r10
	pushq %r11
	pushq %r12
	pushq %r13
	pushq %r14
	pushq %r15
	movq %rsp, %rdi
	cld
	call c_handler
	popq %r15
	popq %r14
	popq %r13
	popq %r12
	popq %r11
	popq %r10
	popq %r9
	popq %rsi
	popq %rdi
	popq %rbp
	popq %rdx
	popq %rcx
	popq %rbx
	popq %rax
	addq $16, %rsp
	iretq
""")

print("table:")
for i in range(INTS_CNT):
	print(r"	.quad entry" + str(i))